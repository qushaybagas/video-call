package ta.face;

import ta.face.R;
import org.doubango.ngn.sip.NgnAVSession;
import org.doubango.ngn.utils.NgnPredicate;
import org.doubango.ngn.utils.NgnStringUtils;

import ta.face.IScreenService;
import ta.face.Screens.BaseScreen;
import ta.face.Screens.IBaseScreen;
import ta.face.Screens.ScreenAV;
import ta.face.Screens.ScreenAVQueue;
import ta.face.Screens.ScreenHome;
import ta.face.Screens.ScreenSplash;
import ta.face.Screens.BaseScreen.SCREEN_TYPE;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class Main extends ActivityGroup {
	private static String TAG = Main.class.getCanonicalName();
	
	public static final int ACTION_NONE = 0;
	public static final int ACTION_RESTORE_LAST_STATE = 1;
	public static final int ACTION_SHOW_AVSCREEN = 2;
	
	private static final int RC_SPLASH = 0;
	
	private Handler mHanler;
	private final Engine mEngine;
	private final IScreenService mScreenService;
	
	public Main(){
		super();
		
		// Set main activity (harus jalan sebelum servis jalan)
		mEngine = (Engine)Engine.getInstance();
		mEngine.setMainActivity(this);
    	mScreenService = ((Engine)Engine.getInstance()).getScreenService();
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        
        mHanler = new Handler();
        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        
        if(!Engine.getInstance().isStarted()){
        	startActivityForResult(new Intent(this, ScreenSplash.class), Main.RC_SPLASH);
        	return;
        }
        
        Bundle bundle = savedInstanceState;
        if(bundle == null){
	        Intent intent = getIntent();
	        bundle = intent == null ? null : intent.getExtras();
        }
        if(bundle != null && bundle.getInt("action", Main.ACTION_NONE) != Main.ACTION_NONE){
        	handleAction(bundle);
        }
        else if(mScreenService != null){
        	mScreenService.show(ScreenHome.class);
        }
    }
    
    @Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
		Bundle bundle = intent.getExtras();
		if(bundle != null){
			handleAction(bundle);
		}
	}
	
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(mScreenService.getCurrentScreen().hasMenu()){
			return mScreenService.getCurrentScreen().createOptionsMenu(menu);
		}
		
		return false;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu){
		if(mScreenService.getCurrentScreen().hasMenu()){
			menu.clear();
			return mScreenService.getCurrentScreen().createOptionsMenu(menu);
		}
		return false;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		IBaseScreen baseScreen = mScreenService.getCurrentScreen();
		if(baseScreen instanceof Activity){
			return ((Activity)baseScreen).onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}
	
	//save state of screen when this app closed
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(mScreenService == null){
			super.onSaveInstanceState(outState);
			return;
		}
		
		IBaseScreen screen = mScreenService.getCurrentScreen();
		if(screen != null){
			outState.putInt("action", Main.ACTION_RESTORE_LAST_STATE);
			outState.putString("screen-id", screen.getId());
			outState.putString("screen-type", screen.getType().toString());
		}
		
		super.onSaveInstanceState(outState);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		this.handleAction(savedInstanceState);
	}
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.d(TAG, "onActivityResult("+requestCode+","+resultCode+")");
		if(resultCode == RESULT_OK){
			if(requestCode == Main.RC_SPLASH){
				Log.d(TAG, "Result from splash screen");
			}
		}
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if(!BaseScreen.processKeyDown(keyCode, event)){
    		return super.onKeyDown(keyCode, event);
    	}
    	return true;
	}
    
    public void exit(){
    	mHanler.post(new Runnable() {
			public void run() {
				if (!Engine.getInstance().stop()) {
					Log.e(TAG, "engine gagal distop");
				}				
				finish();
			}
		});
	}
    
    private void handleAction(Bundle bundle){
		final String id;
		switch(bundle.getInt("action", Main.ACTION_NONE)){
			// Default or ACTION_RESTORE_LAST_STATE
			default:
			case ACTION_RESTORE_LAST_STATE:
				id = bundle.getString("screen-id");
				final String screenTypeStr = bundle.getString("screen-type");
				final SCREEN_TYPE screenType = NgnStringUtils.isNullOrEmpty(screenTypeStr) ? SCREEN_TYPE.HOME_T :
						SCREEN_TYPE.valueOf(screenTypeStr);
				switch(screenType){
					case AV_T:
						mScreenService.show(ScreenAV.class, id);
						break;
					default:
						if(!mScreenService.show(id)){
							mScreenService.show(ScreenHome.class);
						}
						break;
				}
				break;
				
			case ACTION_SHOW_AVSCREEN:
				Log.d(TAG, "Main.ACTION_SHOW_AVSCREEN");
				
				final int activeSessionsCount = NgnAVSession.getSize(new NgnPredicate<NgnAVSession>() {
					public boolean apply(NgnAVSession session) {
						return session != null && session.isActive();
					}
				});
				if(activeSessionsCount > 1){
					mScreenService.show(ScreenAVQueue.class);
				}
				else{
					NgnAVSession avSession = NgnAVSession.getSession(new NgnPredicate<NgnAVSession>() {
						public boolean apply(NgnAVSession session) {
							return session != null && session.isActive() && !session.isLocalHeld() && !session.isRemoteHeld();
						}
					});
					if(avSession == null){
						avSession = NgnAVSession.getSession(new NgnPredicate<NgnAVSession>() {
							public boolean apply(NgnAVSession session) {
								return session != null && session.isActive();
							}
						});
					}
					if(avSession != null){
						if(!mScreenService.show(ScreenAV.class, Long.toString(avSession.getId()))){
							mScreenService.show(ScreenHome.class);
						}
					}
					else{
						Log.e(TAG,"Galal AV sesinya");
						mScreenService.show(ScreenHome.class);
						mEngine.refreshAVCallNotif(R.drawable.phone_call_25);
					}
				}
				break;
				
		}
	}
}