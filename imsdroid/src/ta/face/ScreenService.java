
package ta.face;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

import ta.face.R;
import org.doubango.ngn.services.impl.NgnBaseService;

import ta.face.Engine;
import ta.face.face2face;
import ta.face.IScreenService;
import ta.face.Main;
import ta.face.Screens.IBaseScreen;
import ta.face.Screens.ScreenHome;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.LinearLayout;

public class ScreenService extends NgnBaseService implements IScreenService {
	private final static String TAG = ScreenService.class.getCanonicalName();
	
	private int mLastScreensIndex = -1; // ring cursor
	private final String[] mLastScreens =  new String[]{ // ring
    		null,
    		null,
    		null,
    		null
	};
	
	 
	public boolean start() {
		Log.d(TAG, "starting...");
		return true;
	}

	 
	public boolean stop() {
		Log.d(TAG, "stopping...");
		return true;
	}

	 
	public boolean back() {
		String screen;
		
		// no screen in the stack
		if(mLastScreensIndex < 0){
			return true;
		}
		
		// zero is special case
		if(mLastScreensIndex == 0){
			if((screen = mLastScreens[mLastScreens.length-1]) == null){
				// goto home
				return show(ScreenHome.class);
			}
			else{
				return this.show(screen);
			}
		}
		// all other cases
		screen = mLastScreens[mLastScreensIndex-1];
		mLastScreens[mLastScreensIndex-1] = null;
		mLastScreensIndex--;
		if(screen == null || !show(screen)){
			return show(ScreenHome.class);
		}
		
		return true;
	}

	 
	public boolean bringToFront(int action, String[]... args) {
		Intent intent = new Intent(face2face.getContext(), Main.class);
		try{
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP  | Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("action", action);
			for(String[] arg : args){
				if(arg.length != 2){
					continue;
				}
				intent.putExtra(arg[0], arg[1]);
			}
			face2face.getContext().startActivity(intent);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	 
	public boolean bringToFront(String[]... args) {
		return this.bringToFront(Main.ACTION_NONE);
	}

	 
	public boolean show(Class<? extends Activity> cls, String id) {
		final Main mainActivity = (Main)Engine.getInstance().getMainActivity();
		
		String screen_id = (id == null) ? cls.getCanonicalName() : id;
		Intent intent = new Intent(mainActivity, cls);
		intent.putExtra("id", screen_id);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		final Window window = mainActivity.getLocalActivityManager().startActivity(screen_id, intent);
		if(window != null){
			View view = mainActivity.getLocalActivityManager().startActivity(screen_id, intent).getDecorView();
			
			LinearLayout layout = (LinearLayout) mainActivity.findViewById(R.id.main_linearLayout_principal);
			layout.removeAllViews();
			layout.addView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			
			// add to stack
			this.mLastScreens[(++this.mLastScreensIndex % this.mLastScreens.length)] = screen_id;
			this.mLastScreensIndex %= this.mLastScreens.length;
			return true;
		}
		return false;
	}

	 
	public boolean show(Class<? extends Activity> cls) {
		return this.show(cls, null);
	}

	 
	public boolean show(String id) {
		final  Activity screen = (Activity)((Main)Engine.getInstance().getMainActivity()).getLocalActivityManager().getActivity(id);
		if (screen == null) {
			Log.e(TAG, String.format(
					"Failed to retrieve the Screen with id=%s", id));
			return false;
		} else {
			return this.show(screen.getClass(), id);
		}
	}

	 
	public void runOnUiThread(Runnable r) {
		if(Engine.getInstance().getMainActivity() != null){
			Engine.getInstance().getMainActivity().runOnUiThread(r);
		}
		else{
			Log.e(this.getClass().getCanonicalName(), "No Main activity");
		}
	}

	 
	public boolean destroy(String id) {
		final LocalActivityManager activityManager = (((Main)Engine.getInstance().getMainActivity())).getLocalActivityManager();
		if(activityManager != null){
			activityManager.destroyActivity(id, true);
			
			// http://code.google.com/p/android/issues/detail?id=12359
			// http://www.netmite.com/android/mydroid/frameworks/base/core/java/android/app/LocalActivityManager.java
			try {
				final Field mActivitiesField = LocalActivityManager.class.getDeclaredField("mActivities");
				if(mActivitiesField != null){
					mActivitiesField.setAccessible(true);
					@SuppressWarnings("unchecked")
					final Map<String, Object> mActivities = (Map<String, Object>)mActivitiesField.get(activityManager);
					if(mActivities != null){
						mActivities.remove(id);
					}
					final Field mActivityArrayField = LocalActivityManager.class.getDeclaredField("mActivityArray");
					if(mActivityArrayField != null){
						mActivityArrayField.setAccessible(true);
						@SuppressWarnings("unchecked")
						final ArrayList<Object> mActivityArray = (ArrayList<Object>)mActivityArrayField.get(activityManager);
						if(mActivityArray != null){
							for(Object record : mActivityArray){
								final Field idField = record.getClass().getDeclaredField("id");
								if(idField != null){
									idField.setAccessible(true);
									final String _id = (String)idField.get(record);
									if(id.equals(_id)){
										mActivityArray.remove(record);
										break;
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	 
	public void setProgressInfoText(String text) {
	}

	 
	public IBaseScreen getCurrentScreen() {
		return (IBaseScreen)((Main)Engine.getInstance().getMainActivity()).getLocalActivityManager().getCurrentActivity();
	}

	 
	public IBaseScreen getScreen(String id) {
		return (IBaseScreen)((Main)Engine.getInstance().getMainActivity()).getLocalActivityManager().getActivity(id);
	}
}
