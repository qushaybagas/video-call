
package ta.face.Screens;

import ta.face.R;
import org.doubango.ngn.media.NgnMediaType;
import org.doubango.ngn.services.INgnSipService;
import org.doubango.ngn.utils.NgnStringUtils;

import ta.face.DialerUtils;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

public class ScreenTabDialer  extends BaseScreen {
	private static String TAG = ScreenTabDialer.class.getCanonicalName();
	
	private EditText mEtNumber;
	static enum PhoneInputType{
		Text
	}
	
	private PhoneInputType mInputType;
	private InputMethodManager mInputMethodManager;
	
	private final INgnSipService mSipService;
	
	public ScreenTabDialer() {
		super(SCREEN_TYPE.DIALER_T, TAG);
		
		mSipService = getEngine().getSipService();
		
	}

	 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_tab_dialer);
		
		mEtNumber = (EditText)findViewById(R.id.screen_tab_dialer_editText_number);
		DialerUtils.setDialerImageButton(this, R.id.screen_tab_dialer_button_video, R.drawable.visio_call_48, DialerUtils.TAG_VIDEO_CALL, mOnDialerClick);
		
		//show keyboard
		mInputType = PhoneInputType.Text;
		mEtNumber.setInputType(InputType.TYPE_CLASS_TEXT);
		mEtNumber.setFocusableInTouchMode(true);
		mEtNumber.setFocusable(true);
		
	}
	
	 
	protected void onDestroy() {
       super.onDestroy();
	}
	
	private final View.OnClickListener mOnDialerClick = new View.OnClickListener() {
		 
		public void onClick(View v) {
			int tag = Integer.parseInt(v.getTag().toString());
			final String number = mEtNumber.getText().toString();
			
			//aksi tombol call
			if(mSipService.isRegistered() && !NgnStringUtils.isNullOrEmpty(number)){
				ScreenAV.makeCall(number, NgnMediaType.AudioVideo);
				mEtNumber.setText(NgnStringUtils.emptyValue());
			}
			
		}
	};
	
	 
	public boolean hasBack(){
		return true;
	}
	
	 
	public boolean back(){
		boolean ret = mScreenService.show(ScreenHome.class);
		if(ret){
			mScreenService.destroy(getId());
		}
		return ret;
	}
	
}

