
package ta.face.Screens;

import java.math.BigInteger;

import ta.face.R;
import org.doubango.ngn.services.INgnConfigurationService;
import org.doubango.ngn.utils.NgnConfigurationEntry;
import org.doubango.ngn.utils.NgnStringUtils;
import org.doubango.tinyWRAP.MediaSessionMgr;
import org.doubango.tinyWRAP.tmedia_srtp_mode_t;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

public class ScreenNetwork extends BaseScreen {
	private final static String TAG = ScreenNetwork.class.getCanonicalName();
	
	private final static int REQUEST_CODE_PRIV_KEY = 1234;
	private final static int REQUEST_CODE_PUB_KEY = 12345;
	private final static int REQUEST_CODE_CA = 123456;
	
	private Spinner mSpSRtpMode;
	
	private final static ScreenSecuritySRtpMode sSpinnerSRtpModeItems[] = new ScreenSecuritySRtpMode[] {
		new ScreenSecuritySRtpMode(tmedia_srtp_mode_t.tmedia_srtp_mode_none, "RTP"),
		new ScreenSecuritySRtpMode(tmedia_srtp_mode_t.tmedia_srtp_mode_mandatory, "SRTP"),
	};
	
	private final INgnConfigurationService mConfigurationService;
	
	private EditText mEtProxyHost;
	private EditText mEtProxyPort;
	private Spinner mSpTransport;
	private Spinner mSpProxyDiscovery;
	
	private final static String[] sSpinnerTransportItems = new String[] {NgnConfigurationEntry.DEFAULT_NETWORK_TRANSPORT.toUpperCase(), "TCP", "TLS"/*, "SCTP"*/};
	private final static String[] sSpinnerProxydiscoveryItems = new String[] {NgnConfigurationEntry.DEFAULT_NETWORK_PCSCF_DISCOVERY, NgnConfigurationEntry.PCSCF_DISCOVERY_DNS_SRV/*, "DHCPv4/v6", "Both"*/};
	
	public ScreenNetwork() {
		super(SCREEN_TYPE.NETWORK_T, TAG);
		
		this.mConfigurationService = getEngine().getConfigurationService();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_network);
        
        mEtProxyHost = (EditText)findViewById(R.id.screen_network_editText_pcscf_host);
        mEtProxyPort = (EditText)findViewById(R.id.screen_network_editText_pcscf_port);
        mSpTransport = (Spinner)findViewById(R.id.screen_network_spinner_transport);
        mSpProxyDiscovery = (Spinner)findViewById(R.id.screen_network_spinner_pcscf_discovery);
        
        // spinners
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sSpinnerTransportItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpTransport.setAdapter(adapter);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sSpinnerProxydiscoveryItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpProxyDiscovery.setAdapter(adapter);
        
        mEtProxyHost.setText(mConfigurationService.getString(NgnConfigurationEntry.NETWORK_PCSCF_HOST, NgnConfigurationEntry.DEFAULT_NETWORK_PCSCF_HOST));
        mEtProxyPort.setText(Integer.toString(mConfigurationService.getInt(NgnConfigurationEntry.NETWORK_PCSCF_PORT, NgnConfigurationEntry.DEFAULT_NETWORK_PCSCF_PORT)));
        mSpTransport.setSelection(super.getSpinnerIndex(
				mConfigurationService.getString(NgnConfigurationEntry.NETWORK_TRANSPORT, sSpinnerTransportItems[0]),
				sSpinnerTransportItems));
        mSpProxyDiscovery.setSelection(super.getSpinnerIndex(
				mConfigurationService.getString(NgnConfigurationEntry.NETWORK_PCSCF_DISCOVERY, sSpinnerProxydiscoveryItems[0]),
				sSpinnerProxydiscoveryItems));
        
     // get controls
        mSpSRtpMode = (Spinner)findViewById(R.id.screen_security_spinner_srtp_modes);
        
        // load values from configuration file (do it before adding UI listeners)
        ArrayAdapter<ScreenSecuritySRtpMode> Srtpadapter = new ArrayAdapter<ScreenSecuritySRtpMode>(this, android.R.layout.simple_spinner_item, ScreenNetwork.sSpinnerSRtpModeItems);
        Srtpadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpSRtpMode.setAdapter(Srtpadapter);
        
        mSpSRtpMode.setSelection(ScreenSecuritySRtpMode.getSpinnerIndex(tmedia_srtp_mode_t.valueOf(mConfigurationService.getString(
				NgnConfigurationEntry.SECURITY_SRTP_MODE,
				NgnConfigurationEntry.DEFAULT_SECURITY_SRTP_MODE))));
        
        
        // add listeners (for the configuration)
        super.addConfigurationListener(mEtProxyHost);
        super.addConfigurationListener(mEtProxyPort);
        super.addConfigurationListener(mSpTransport);
        super.addConfigurationListener(mSpProxyDiscovery);
        super.addConfigurationListener(mSpSRtpMode);
	}
	
	protected void onPause() {
		if(super.mComputeConfiguration){
			
			mConfigurationService.putString(NgnConfigurationEntry.NETWORK_PCSCF_HOST, 
					mEtProxyHost.getText().toString().trim());
			mConfigurationService.putInt(NgnConfigurationEntry.NETWORK_PCSCF_PORT, 
					NgnStringUtils.parseInt(mEtProxyPort.getText().toString().trim(), NgnConfigurationEntry.DEFAULT_NETWORK_PCSCF_PORT) );
			mConfigurationService.putString(NgnConfigurationEntry.NETWORK_TRANSPORT, 
					ScreenNetwork.sSpinnerTransportItems[mSpTransport.getSelectedItemPosition()]);
			mConfigurationService.putString(NgnConfigurationEntry.NETWORK_PCSCF_DISCOVERY, 
					ScreenNetwork.sSpinnerProxydiscoveryItems[mSpProxyDiscovery.getSelectedItemPosition()]);
			mConfigurationService.putString(NgnConfigurationEntry.SECURITY_SRTP_MODE, 
					sSpinnerSRtpModeItems[mSpSRtpMode.getSelectedItemPosition()].mMode.toString());
			
			
			// Compute
			if(!mConfigurationService.commit()){
				Log.e(TAG, "Failed to commit() configuration");
			}
			else{
				MediaSessionMgr.defaultsSetSRtpMode(sSpinnerSRtpModeItems[mSpSRtpMode.getSelectedItemPosition()].mMode);
			}
			
			super.mComputeConfiguration = false;
		}
		super.onPause();
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		if (requestCode == ScreenNetwork.REQUEST_CODE_PRIV_KEY) {
			Uri uri = data.getData();
			Log.d(TAG, uri.toString());
		}
		else if (requestCode == ScreenNetwork.REQUEST_CODE_PUB_KEY) {
			
		}
		else if (requestCode == ScreenNetwork.REQUEST_CODE_CA) {

		}
		else{
			Log.e(ScreenNetwork.TAG, String.format("%d is an unknown request code", requestCode));
		}
	}
	
	private static class ScreenSecuritySRtpMode {
		private final String mDescription;
		private final tmedia_srtp_mode_t mMode;

		private ScreenSecuritySRtpMode(tmedia_srtp_mode_t mode, String description) {
			mMode = mode;
			mDescription = description;
		}

		@Override
		public String toString() {
			return mDescription;
		}

		@Override
		public boolean equals(Object o) {
			return mMode.equals(((ScreenSecuritySRtpMode)o).mMode);
		}
		
		static int getSpinnerIndex(tmedia_srtp_mode_t mode){
			for(int i = 0; i< sSpinnerSRtpModeItems.length; i++){
				if(mode == sSpinnerSRtpModeItems[i].mMode){
					return i;
				}
			}
			return 0;
		}
	}
}
