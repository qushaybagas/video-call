# README #

last update : August 2012

### What is this repository for? ###

* Mobile app for realtime video call communication. Conducted IMS implementation with OpenIMS, and the client application developed based on Android.  And also, in this study  will be comparing  the  performance  of  implementation  of  data  security  on  client  and server  network  using  SRTP  transport  protocol  and  no  security  implementation with  RTP  transport  protocol.  The  data  examined  in  this  study  is  the  video data using H.263 protocol and voice data using G.711 protocol.
* Version : 1.0

### How do I get set up? ###

* Setup / install the IMS Server (IP Multimedia Subsytem Server) - HSS & CSCF [http://www.openimscore.org/](http://www.openimscore.org/) on your Linux machine
* Clone / download this repo and import on your eclipse
* Config Server IP on java script

### Who do I talk to? ###

* Repo owner : bagas.qushay@gmail.com